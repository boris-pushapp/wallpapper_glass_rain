package ido.should.why.thisheeet

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface MyApi {
    @GET("photos")
    fun getImgList(@Query("client_id") str: String, @Query("per_page") size: String): Call<List<HashMap<String, Any>>>

    companion object {
        fun create(): MyApi {
            val builder = Retrofit.Builder().baseUrl("https://api.unsplash.com/collections/1410320/").addConverterFactory(GsonConverterFactory.create()).build()
            return builder.create(MyApi::class.java)
        }
    }

}